### SCANPY
Крупномасштабный одноклеточный анализ данных экспрессии генов 

### Команда:
- Екатерина Веденеева
- Ольга Ковалёва
- Дмитрий Сердюков
- Матвей Четвергов

### Структура репозитория
- Файл "scanpy_presentation.mp4" - видео-доклад 
- В файле "preprocessing.ipynb" - основные методы предобработки данных
- В файле "visualization_and_clustering.ipynb" - основные методы визуализации и кластеризации 
- В папке "comparison" находятся файлы, необходимые для сравнения пакета с аналогами
- В файле "SCANPY.ipynb" находится тестовый кейс
